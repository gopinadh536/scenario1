variable "aws_region" {
  description = "aws region"
}

variable "environment" {
  description = "The Deployment environment"
}
variable "vpc_id" {
  description = "The vpc id"
}

//Networking
variable "iam_fleet_role" {
  description = "IAM role"
}

variable "public_subnet" {
  description = "public subnet"
}

variable "private_subnets_cidr" {
  type        = list
  description = "The CIDR block for the private subnet"
}

variable "availability_zones" {
  type        = list
  description = "list of AZs"
}
variable "lb_public_subnets" {
  type        = list
  description = "list of AZs"
}
