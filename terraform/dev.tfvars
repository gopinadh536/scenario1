//AWS 
aws_region      = "ap-southeast-1"
environment = "dev"
public_subnet = "subnet-e668adbf"
iam_fleet_role = "arn:aws:iam::028563737824:role/scenario_role1"
vpc_id            = "vpc-6fd1f408"
private_subnets_cidr = ["172.31.51.0/24","172.31.52.0/24","172.31.53.0/24"] //List of private subnet cidr range
availability_zones = ["ap-southeast-1a","ap-southeast-1b","ap-southeast-1c"]
lb_public_subnets = ["subnet-e668adbf","subnet-52ccf21b"]
