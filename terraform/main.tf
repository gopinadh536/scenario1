provider "aws" {
  version = "~> 2.70"
  region = "${var.aws_region}"
}


/* Elastic IP for NAT */
resource "aws_eip" "nat_eip" {
  vpc        = true
}
/* NAT */
resource "aws_nat_gateway" "nat" {
  allocation_id = "${aws_eip.nat_eip.id}"
  subnet_id     = "${var.public_subnet}"
  tags = {
    Name        = "nat"
  }
}

/* Private subnet */
resource "aws_subnet" "private_subnet" {
  vpc_id                  = "${var.vpc_id}"
  count                   = "${length(var.private_subnets_cidr)}"
  cidr_block              = "${element(var.private_subnets_cidr, count.index)}"
  availability_zone       = "${element(var.availability_zones,   count.index)}"
  map_public_ip_on_launch = false
  tags = {
    Name        = "${var.environment}-${element(var.availability_zones, count.index)}-private-subnet"
    Environment = "${var.environment}"
  }
}
/* Routing table for private subnet */
resource "aws_route_table" "private" {
  vpc_id = "${var.vpc_id}"
  tags = {
    Name        = "${var.environment}-private-route-table"
  }
}


resource "aws_route" "private_nat_gateway" {
  route_table_id         = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.nat.id}"
}
/* Route table associations */
resource "aws_route_table_association" "private" {
  count          = "${length(var.private_subnets_cidr)}"
  subnet_id      = "${element(aws_subnet.private_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.private.id}"
}


resource "aws_security_group" "allow_web" {
  name        = "allow_web"
  description = "Allow web inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description      = "http from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
      }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_web"
  }
}
/*
resource "aws_launch_template" "foo" {
  name          = "inc-dec-launch-template"
  image_id      = "ami-0af2f764c580cc1f9"
  instance_type = "t3.micro"
  key_name      = "inc-dec"
}

resource "aws_spot_fleet_request" "foo" {
  iam_fleet_role  = var.iam_fleet_role
  spot_price      = "0.005"
  target_capacity = 3
 

  launch_template_config {
    launch_template_specification {
      id      = aws_launch_template.foo.id
      version = aws_launch_template.foo.latest_version
    }
    overrides {
      subnet_id = "${element(aws_subnet.private_subnet.*.id, 0)}"
    }
    overrides {
      subnet_id = "${element(aws_subnet.private_subnet.*.id, 0)}"
      #subnet_id = aws_subnet.private_subnet.id[count.index[1]]
    }
    overrides {
      subnet_id = "${element(aws_subnet.private_subnet.*.id, 0)}"
      #subnet_id = aws_subnet.private_subnet.id[count.index[2]]
    }
    #vpc_security_group_ids = aws_security_group.allow_web.id
  }

  depends_on = [aws_security_group.allow_web]
}
*/




resource "aws_launch_template" "s1-inc-lt" {
  name_prefix            = "s1-inc-lt"
  image_id               = "ami-0af2f764c580cc1f9"
  instance_type          = "t2.micro"
  key_name               = "inc-dec"
  vpc_security_group_ids = ["${aws_security_group.allow_web.id}"]
  #instance_market_options {
  #  market_type = "spot"
  #}

  iam_instance_profile {
    name = "scenario_role1"
  }

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size           = 10
      volume_type           = "gp2"
      encrypted             = true
      delete_on_termination = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
  user_data = filebase64("${path.module}/userdata.sh")

}

resource "aws_lb" "inc-alb" {
  name               = "s1-inc-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = var.lb_public_subnets
  security_groups    = ["${aws_security_group.allow_web.id}"]

}

resource "aws_autoscaling_group" "s1-inc-asg" {
  name                = "s1-inc-asg"
  desired_capacity    = 3
  max_size            = 6
  min_size            = 3
  force_delete        = true
  depends_on          = [aws_lb.inc-alb]
  target_group_arns   = ["${aws_lb_target_group.s1-inc-tg.arn}"]
  health_check_type   = "EC2"
  vpc_zone_identifier = aws_subnet.private_subnet.*.id

  launch_template {
    id      = aws_launch_template.s1-inc-lt.id
    version = "$Latest"
  }

  tag {
    key                 = "Name"
    value               = "s1-inc-asg"
    propagate_at_launch = true
  }
}

resource "aws_lb_target_group" "s1-inc-tg" {
  name       = "s1-inc-tg"
  port       = 80
  protocol   = "HTTP"
  vpc_id     = var.vpc_id
  health_check {
    interval            = 60
    path                = "/index.html"
    port                = 80
    healthy_threshold   = 2
    unhealthy_threshold = 4
    timeout             = 30
    protocol            = "HTTP"
    matcher             = "200,202"
  }

}

resource "aws_lb_listener" "s1-inc-http" {
  load_balancer_arn = aws_lb.inc-alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.s1-inc-tg.arn
  }
}
