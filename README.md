# Scenario1



## Current Deployment Pipeline

Gitlab - > S3 -> EC2

## Current Application Traffic flow

User -> ALB -> EC2

## Suggestions to Current architecture

1. Deploy and configure S3 as static website Instead of deploying the static web page to EC2 instances
2. Enable CDN if we need to attach the SSL/TLS certificate or Cache response

### Benefites of suggested architecture
1. S3 cost is lesser compare to EC2
2. Cost will be based on no of requests Instead of compute
3. Steps like ASG refresh not needed so deployment speed will be better
4. Complex Network architecture such as private/public subnets configuration could be avoided for this application



![Original screenshot](Capture.PNG  )


![Upper case screenshot](Capture_uppercase.PNG )
